FROM python:3.8.3-slim-buster

ARG PORT=8080
ARG HOST="0.0.0.0"
COPY requirements.txt /requirements.txt
RUN python3 -m pip install -r /requirements.txt
COPY src/ /src

EXPOSE 8080
WORKDIR src/
CMD ["gunicorn", "-b", "0.0.0.0:8080", "-k", "gevent", "wsgi:app"]

