import os
from flask import request, jsonify
from flask import Flask
import logging

app = Flask(__name__)
logging.basicConfig(level=logging.DEBUG)


def do_backend_stuff(ip_address):

    """
    Gets the number of integers in an IP address to demonstrate how a Flask application
    can call other python functions when handling a web request.
    Maybe this could be a call to another database? Or some other API?
    :param ip_address:
    :return:
    """
    app.logger.info(f"Analysing {ip_address} for number of integers")
    return sum([1 if x.isnumeric() else 0 for x in ip_address])


def get_client_ip(request_object: request):

    """
    Gets ip from remote address or from forwarded address if exists (if you're behind a load balancer
    then the ip address under "remote_addr_ is the ip of the load balancer)
    :param request_object:
    :return: ip address string
    """

    if request_object.environ.get('HTTP_X_ORIGINAL_FORWARDED_FOR') is None:
        response_ip = request_object.environ.get('REMOTE_ADDR')
    else:
        response_ip = request_object.environ.get('HTTP_X_ORIGINAL_FORWARDED_FOR')

    return response_ip


@app.route('/request')
def get_userdata():

    get_useragent = str(request.user_agent)
    get_ip = get_client_ip(request_object=request)
    processed_data = do_backend_stuff(get_ip)
    app.logger.info("Data Processing complete")
    return jsonify(dict(useragent=get_useragent,
                        client_ip=get_ip,
                        numbers_in_client_ip=processed_data,
                        message="This is a JSON message responding "
                                "to your web request with your ip and "
                                "the identifier of your device type"))


if __name__ == '__main__':

    host = os.environ.get("HOST", "localhost")
    port = os.environ.get("PORT", "8080")
    app.logger.warning(f"\nStarting Application ...\nHost: {host}\nPort: {port}")
    app.run(host=host, port=port)

